# -*- coding: utf-8 -*-

import sys

from urllib.request import urlopen
from urllib.parse import urlencode
from urllib.error import HTTPError, URLError

class SMSSender():

    urlfreesms = "https://smsapi.free-mobile.fr/sendmsg?%s"

    messages_dic = {
        200: "Le SMS a bien été envoyé",
        400: "Un des paramètres obligatoire est manquant",
        402: "Trop de SMS ont été envoyés en trop peu de temps",
        403: "Le service n'est pas activé sur l'espace abonné, ou login / clé incorrect",
        500: "Erreur côté serveur. Veuillez essayer ultérieurement",

        404: "Erreur dans l'url %s. Cette dernière pourrait avoir changé" % urlfreesms[:-2],
         -1: "Code de retour HTTP inconnu: %d",
         -2: "Erreur non prévue: %s",
         -3: "Pas de message, la chaîne est vide!"
    }

    def __init__(self, user, key, msg=''):

        # attributes initialized in __init__
        self.user = user
        self.key = key

        # attributes which are initialized later
        self.code=None
        self.response=''

        if msg:
            self.send(msg)
        else:
            self.msg = msg

    def send(self, msg=''):

        self.msg = msg

        # params for urlencode
        param = {'user': self.user,
                 'pass': self.key,
                 'msg': self.msg
         }

        if not msg:
            self.code = -3

        else:

            try:
                response = urlopen(SMSSender.urlfreesms % urlencode(param))

                # Récupération du code de retour HTTP (200, ... ? )
                self.code = response.getcode()

            except HTTPError as err:
                # Récupération du code d'erreur HTTP (403, ... ?)
                self.code = err.code

            except URLError as err:
                self.code = 404

            except:  # not planned
                self.code = -2


        if not self.code in SMSSender.messages_dic.keys():
            unknown_code = self.code
            self.code = -1  # Unknown HTTP code
            self.response = SMSSender.messages_dic.get(self.code) % unknown_code

        elif self.code == -2:  # not planned
            self.response = SMSSender.messages_dic.get(self.code) % sys.exc_info()[0]

        else:  # we control the response code
            self.response = SMSSender.messages_dic.get(self.code)



if __name__ == '__main__':

    import argparse

    help_messages = {
        'description': "Envoi de SMS avec Free Mobile sur votre propre portable",
        'user': "Identifiant Free Mobile",
        'key': "Clé d'identification",
        'msg': "Message à envoyer",
        'help': "Affiche ce message d'aide et se termine."
    }

    # Command line parsing
    parse_args = {

        'user': ('u', dict(action="store", dest="user",
                             help=help_messages['user'], required=True)),

        'key': ('k', dict(action="store", dest="key",
                             help=help_messages['key'], required=True)),

        'msg': ('m', dict(action="store", dest="msg",
                             help=help_messages['msg'], required=True)),

        'help': ('h', dict(action="help", default=argparse.SUPPRESS,
                            help=help_messages['help'])),
        # cf. https://stackoverflow.com/a/35848313/8496767

    }

    parser = argparse.ArgumentParser(description=help_messages['description'], add_help=False)

    for k, v in parse_args.items():
        parser.add_argument('-%s'%v[0], '--%s'%k, **v[1])

    results = parser.parse_args()

    sender = SMSSender(results.user, results.key, results.msg)

    print(sender.response, sender.code)
