# SMS Free Mobile

Envoi de SMS avec Free Mobile sur votre propre portable

*Vous devez activer ce service depuis votre espace abonné Free Mobile* :

> Gérer mon compte > Mes options > Notifications par SMS

Une fois le service activé, vous obtenez votre clé d'identification

Utilisation:
```
smsfreemobile.py [-h] -u USER -k KEY -m MSG

optional arguments:

 -h, --help            show this help message and exit
 -u USER, --user USER  Identifiant Free Mobile
 -k KEY, --key KEY     Clé d'identification
 -m MSG, --msg MSG     Message à envoyer
```

Exemple d'utilisation :

`python3 smsfreemobile.py -u 12345678 -k OcRojxSrhGKPzx --msg "Mon message !"`

remarque:

    pour python3.x

TODO:

+ écrire les messages dans un fichier log
+ documenter la classe avec des docstrings

